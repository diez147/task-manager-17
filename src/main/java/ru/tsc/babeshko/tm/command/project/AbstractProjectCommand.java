package ru.tsc.babeshko.tm.command.project;

import ru.tsc.babeshko.tm.api.service.IProjectService;
import ru.tsc.babeshko.tm.api.service.IProjectTaskService;
import ru.tsc.babeshko.tm.command.AbstractCommand;
import ru.tsc.babeshko.tm.enumerated.Status;
import ru.tsc.babeshko.tm.model.Project;
import ru.tsc.babeshko.tm.util.DateUtil;

import java.util.List;

public abstract class AbstractProjectCommand extends AbstractCommand {

    public IProjectService getProjectService() {
        return getServiceLocator().getProjectService();
    }

    public IProjectTaskService getProjectTaskService() {
        return getServiceLocator().getProjectTaskService();
    }

    public void renderProjects(List<Project> projects) {
        int index = 1;
        for (final Project project : projects) {
            if (project == null) continue;
            System.out.println(index + "." + project);
            index++;
        }
    }

    protected void showProject(final Project project) {
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + Status.toName(project.getStatus()));
        System.out.println("CREATED: " + DateUtil.toString(project.getCreated()));
        System.out.println("DATE BEGIN: " + DateUtil.toString(project.getDateBegin()));
        System.out.println("DATE END: " + DateUtil.toString(project.getDateEnd()));
    }

}
