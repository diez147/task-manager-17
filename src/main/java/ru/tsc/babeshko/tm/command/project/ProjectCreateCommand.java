package ru.tsc.babeshko.tm.command.project;

import ru.tsc.babeshko.tm.util.TerminalUtil;

import java.util.Date;

public final class ProjectCreateCommand extends AbstractProjectCommand {

    public static final String NAME = "proj-create";

    public static final String DESCRIPTION = "Create new project.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT CREATE]");
        System.out.println("[ENTER NAME:]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION:]");
        final String description = TerminalUtil.nextLine();
        System.out.println("[ENTER DATE BEGIN:]");
        final Date dateBegin = TerminalUtil.nextDate();
        System.out.println("[ENTER DATE END:]");
        final Date dateEnd = TerminalUtil.nextDate();
        getProjectService().create(name, description, dateBegin, dateEnd);
    }

}