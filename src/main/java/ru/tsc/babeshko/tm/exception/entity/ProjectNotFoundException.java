package ru.tsc.babeshko.tm.exception.entity;

public final class ProjectNotFoundException extends AbstractEntityNotFoundException{

    public ProjectNotFoundException() {
        super("Error! Project not found...");
    }

}
