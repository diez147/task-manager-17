package ru.tsc.babeshko.tm.exception.field;

import ru.tsc.babeshko.tm.exception.AbstractException;

public class AbstractFieldException extends AbstractException {

    public AbstractFieldException() {
    }

    public AbstractFieldException(String message) {
        super(message);
    }

    public AbstractFieldException(String message, Throwable cause) {
        super(message, cause);
    }

    public AbstractFieldException(Throwable cause) {
        super(cause);
    }

    public AbstractFieldException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
