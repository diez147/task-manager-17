package ru.tsc.babeshko.tm.exception.entity;

public final class TaskNotFoundException extends AbstractEntityNotFoundException{

    public TaskNotFoundException() {
        super("Error! Task not found...");
    }

}
