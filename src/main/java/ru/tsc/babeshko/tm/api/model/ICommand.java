package ru.tsc.babeshko.tm.api.model;

public interface ICommand {

    String getName();

    String getDescription();

    String getArgument();

}
