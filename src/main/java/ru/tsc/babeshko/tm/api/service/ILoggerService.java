package ru.tsc.babeshko.tm.api.service;

public interface ILoggerService {

    void info (String message);

    void command (String message);

    void error (Exception e);

    void debug (String message);

}
