package ru.tsc.babeshko.tm.api.service;

public interface IServiceLocator {

    ICommandService getCommandService();

    ITaskService getTaskService();

    IProjectService getProjectService();

    IProjectTaskService getProjectTaskService();

    ILoggerService getLoggerService();

}
