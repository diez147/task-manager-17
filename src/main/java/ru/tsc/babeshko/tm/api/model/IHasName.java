package ru.tsc.babeshko.tm.api.model;

public interface IHasName {

    String getName();

    void setName(String name);

}
